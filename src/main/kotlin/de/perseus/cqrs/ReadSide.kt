package de.perseus.cqrs

import de.perseus.UserCreated
import de.perseus.UserEmailChanged
import de.perseus.UserEmailValidated
import de.perseus.UserEvent

data class User(val email: String, val verified: Boolean)

object EventHandler {

    // this could also be a DB, or a cache
    private val optimizedRepresentation: MutableMap<String, User> = mutableMapOf()

    // Is called every time a new event is put into the event store
    fun handleEvent(userEvent: UserEvent) {
        when (userEvent) {
            is UserCreated -> optimizedRepresentation[userEvent.email] = User(userEvent.email, false)
            is UserEmailValidated -> optimizedRepresentation[userEvent.email] = User(userEvent.email, true)
            is UserEmailChanged -> {
                optimizedRepresentation.remove(userEvent.oldEmail)
                optimizedRepresentation[userEvent.newEmail] = User(userEvent.newEmail, false)
            }

            else -> {}
        }
    }

    // GET /users/{email}/verified
    fun isUserVerified(email: String): Boolean {
        return optimizedRepresentation[email]?.verified ?: false
    }

    // GET /users?verified=true
    fun verifiedUsers(): List<User> {
        return optimizedRepresentation.values.filter { u -> u.verified }
    }

}