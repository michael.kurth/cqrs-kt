package de.perseus.cqrs

import de.perseus.*
import java.util.UUID

data class CreateUserCommand(val email: String, val metaInfo: MetaInfo)
data class ValidateUserEmailCommand(val token: String, val metaInfo: MetaInfo)
data class ChangeUserEmailCommand(val oldEmail: String, val newEmail: String, val metaInfo: MetaInfo)


object CommandHandler {

    // our external system to save events in
    private val eventStore: MutableList<UserEvent> = mutableListOf()

    // local read side to validate properly
    private val knownEmails: MutableList<String> = mutableListOf()
    private val knownTokens: MutableMap<String, String> = mutableMapOf()


    // POST /users
    fun createUser(user: CreateUserCommand) {
        doRandomValidation(user)
        sendConfirmationEmail(user)
        knownEmails.add(user.email)
        knownTokens[UUID.randomUUID().toString()] = user.email
        eventStore.add(UserCreated(user.email, user.metaInfo))
    }

    // POST /confirmations/{token}
    fun confirmEmail(validateEmail: ValidateUserEmailCommand) {
        val maybeEmail = knownTokens[validateEmail.token]
        if (maybeEmail != null) {
            knownTokens.remove(validateEmail.token)
            eventStore.add(UserEmailValidated(maybeEmail, validateEmail.metaInfo))
        }
    }

    // PUT /users/{email}
    fun changeEmail(changeUserEmail: ChangeUserEmailCommand) {
        if (knownEmails.contains(changeUserEmail.oldEmail)) {
            replaceEmail(changeUserEmail)
            eventStore.add(
                UserEmailChanged(
                    oldEmail = changeUserEmail.oldEmail,
                    newEmail = changeUserEmail.newEmail,
                    metaInfo = changeUserEmail.metaInfo
                )
            )
        }
    }

    private fun replaceEmail(changeUserEmail: ChangeUserEmailCommand) {
        knownEmails.add(changeUserEmail.newEmail)
        knownEmails.remove(changeUserEmail.oldEmail)
        knownTokens[UUID.randomUUID().toString()] = changeUserEmail.newEmail
    }

    private fun doRandomValidation(createUserCommand: CreateUserCommand) {}
    private fun sendConfirmationEmail(createUserCommand: CreateUserCommand) {}
}