package de.perseus

import java.time.ZonedDateTime

data class MetaInfo(val time: ZonedDateTime, val who: String, val why: String)

sealed interface UserEvent
data class UserCreated(val email: String, val metaInfo: MetaInfo) : UserEvent
data class UserEmailValidated(val email: String, val metaInfo: MetaInfo) : UserEvent
data class UserPasswordSet(val userId: Int, val hashedPassword: String, val metaInfo: MetaInfo) : UserEvent
data class UserEmailChanged(val oldEmail: String, val newEmail: String, val metaInfo: MetaInfo) : UserEvent